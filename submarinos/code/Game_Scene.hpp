/*
 * GAME SCENE
 * Copyright 2019 Javier Estefania
 */

#ifndef GAME_SCENE_HEADER
#define GAME_SCENE_HEADER

    #include <map>
    #include <list>
    #include <memory>
    #include <basics/Canvas>
    #include <basics/Id>
    #include <basics/Scene>
    #include <basics/Texture_2D>
    #include <basics/Timer>

    #include "Sprite.hpp"

    namespace example
    {

        using basics::Id;
        using basics::Timer;
        using basics::Canvas;
        using basics::Texture_2D;

        class Game_Scene : public basics::Scene
        {

            // Those typedef makes the code easier for us
            typedef std::shared_ptr < Sprite     >     Sprite_Handle;
            typedef std::list< Sprite_Handle     >     Sprite_List;
            typedef std::shared_ptr< Texture_2D  >     Texture_Handle;
            typedef std::map< Id, Texture_Handle >     Texture_Map;
            typedef basics::Graphics_Context::Accessor Context;


            //state of the scene
            enum State
            {
                LOADING,
                RUNNING,
                ERROR
            };

            //states in the game
            enum Gameplay_State
            {
                UNINITIALIZED,
                WAITING_TO_START,
                PLAYING,
                LOST,
                PAUSING,

            };

        private:
            //array textures
            static struct   Texture_Data { Id id; const char * path; } textures_data[];

            //number array textures
            static unsigned textures_count;



        private:

            State          state;                               //state of the scene
            Gameplay_State gameplay;                            //Scene running
            bool           suspended;                           //pause
            //canvas width and heigth
            unsigned       canvas_width;
            unsigned       canvas_height;


            //texture map where the texture are load
            Texture_Map    textures;
            // Where the sprite are save
            Sprite_List    sprites;

            // all the sprites
            Sprite * left_border;
            Sprite * rigth_border;
            Sprite * tank;
            Sprite * tank2;
            Sprite * player;
            Sprite * bomba;
            Sprite * health;
            Sprite * health1;
            Sprite * health2;
            Sprite * lostgame;
            Sprite * pauseplay;
            Sprite * pauseicon;
            Sprite * exiticon;
            // bool to know if the screen is being touched or not
            bool mouse;
            //the time until you can trowh the next bomb
            bool colddown;

            //mouse position
            float    user_target_y;
            float    user_target_x;
            //timing
            Timer    timer;

        public:

            Game_Scene();

            basics::Size2u get_view_size () override
            {
                return { canvas_width, canvas_height };
            }

            bool initialize () override;


            void suspend () override;


            void resume () override;

            void handle (basics::Event & event) override;

            void update (float time) override;

            void render (Context & context) override;

        private:

            void load_textures ();

            void pause ();
//when you want to continue when is pause
            void continuepause ();

            void create_sprites ();

            void restart_game ();

            void lost ();

            void start_playing ();

            void run_simulation (float time);

            void update_ai ();

            void update_user ();

            void collision_bomb();

            void render_loading (Canvas & canvas);

            void render_playfield (Canvas & canvas);

        };

    }

#endif
