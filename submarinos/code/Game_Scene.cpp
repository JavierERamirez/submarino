/*
 * GAME SCENE
 * Copyright 2020 Javier Estefania
 */

#include "Game_Scene.hpp"
#include "Menu_Scene.hpp"
#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>
#include <math.h>
#include <stdlib.h>




using namespace basics;
using namespace std;

namespace example
{

    //load of the textures for this scene
    Game_Scene::Texture_Data Game_Scene::textures_data[] =
    {
        { ID(loading),    "ingame/loading.png"},
        { ID(vbar),       "ingame/limit.png"},
        { ID(player), "ingame/helicoptero.png"},
        { ID(iasub), "ingame/tank.png"},
        { ID(fondo), "ingame/fondo.jpg"},
        { ID(bomb), "ingame/bomb.png"},
        { ID(healthsp), "ingame/corazon.png"},
        { ID(lostsp), "ingame/lost.png"},
        { ID(pausesp), "ingame/pause.png"},
        { ID(pauseiconsp), "ingame/pausa.png"},
        { ID(exitsp), "ingame/exit.png"},


    };

    // Pâra determinar el número de items en el array textures_data, se divide el tamaño en bytes
    // del array completo entre el tamaño en bytes de un item:

    unsigned Game_Scene::textures_count = sizeof(textures_data) / sizeof(Texture_Data);

    // ---------------------------------------------------------------------------------------------
    // Definiciones de los atributos estáticos de la clase:


    float  tank_speed=200;
    float player_speed=300;
    float bomb_speed=-300;
    int contvidas=0;
    Vector2f mouseposition;

    // ---------------------------------------------------------------------------------------------

    Game_Scene::Game_Scene()
    {
        canvas_width  = 1280;
        canvas_height =  720;
        // Se inicia la semilla del generador de números aleatorios:
       // srand (unsigned(time(nullptr)));

        initialize ();
    }

    // ---------------------------------------------------------------------------------------------
    // Algunos atributos se inicializan en este método en lugar de hacerlo en el constructor porque
    // este método puede ser llamado más veces para restablecer el estado de la escena y el constructor
    // solo se invoca una vez.

    bool Game_Scene::initialize ()
    {
        state     = LOADING;
        suspended = true;
        gameplay  = UNINITIALIZED;

        return true;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::suspend ()
    {
        suspended = true;               // Se marca que la escena ha pasado a primer plano
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::resume ()
    {
        suspended = false;              // Se marca que la escena ha pasado a segundo plano
    }

    void Game_Scene::pause() {

        pauseplay->set_position({canvas_width/2, (canvas_height / 2.f)+200.f});
        exiticon->set_position({canvas_width/2, (canvas_height / 2.f)});
        player->set_speed_x(0);
        tank->set_speed_x(0);
        tank2->set_speed_x(0);
        bomba->set_speed_y(0);
        exiticon->show();
        pauseplay->show();
        gameplay=PAUSING;


    }
    void Game_Scene::continuepause() {

        player->set_speed_x(player_speed);
        tank->set_speed_x(tank_speed);
        tank2->set_speed_x(tank_speed);
        bomba->set_speed_y(bomb_speed);
        pauseplay->hide();
        exiticon->hide();
        exiticon->set_position({2000.f,2000.f});
        gameplay=PLAYING;

    }
    // ---------------------------------------------------------------------------------------------

    void Game_Scene::handle (Event & event)
    {
        if (state == RUNNING)               // Se descartan los eventos cuando la escena está LOADING
        {
            if (gameplay == WAITING_TO_START)
            {
                start_playing ();           // Se empieza a jugar cuando el usuario toca la pantalla por primera vez
            }
            else switch (event.id)
            {
                case ID(touch-started):     // El usuario toca la pantalla
                case ID(touch-moved):
                {
                    user_target_y = *event[ID(y)].as< var::Float > ();
                    user_target_x = *event[ID(x)].as< var::Float > ();
                    mouse = true;
                    mouseposition = {user_target_x, user_target_y};
                    break;
                }

                case ID(touch-ended):       // El usuario deja de tocar la pantalla
                {
                    mouse = false;
                    player->set_speed_x  (0.f);
                    break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::update (float time)
    {
        if (!suspended) switch (state)
        {

            case LOADING: load_textures  ();
                break;
            case RUNNING: run_simulation (time);
                break;
            case ERROR:
                break;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render (Context & context)
    {
        if (!suspended)
        {
            //creates the canvas
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));
            //checks if the canvas is created or not
            if (!canvas)
            {
                 canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            // Si el canvas se ha podido obtener o crear, se puede dibujar con él:

            if (canvas)
            {
                canvas->clear ();

                switch (state)
                {
                    case LOADING: render_loading   (*canvas); break;
                    case RUNNING: render_playfield (*canvas); break;
                    case ERROR:   break;
                }
            }
        }
    }

   //Load the neccesary textures.
    void Game_Scene::load_textures ()
    {
        if (textures.size () < textures_count)          // Si quedan texturas por cargar...
        {
            // In this case you need a context where de textures could update

            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                //Load the textures                         how many texturex have been loaded
                Texture_Data   & texture_data = textures_data[textures.size ()];
                Texture_Handle & texture      = textures[texture_data.id] = Texture_2D::create (texture_data.id, context, texture_data.path);
               //Checks if there is an error
                if (texture) context->add (texture); else state = ERROR;


            }
        }
        else{
            if (timer.get_elapsed_seconds () > 1.f)
            {
                //if textures are loaded too early it waits 1 second
                create_sprites ();
                restart_game   ();
                // change to running state
                state = RUNNING;
            }
        }

    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::create_sprites ()
    {
        //Creates the scenary and the limits
        Sprite_Handle    left_limit(new Sprite( textures[ID(vbar)].get () ));
        Sprite_Handle    right_bar(new Sprite( textures[ID(vbar)].get () ));
        Sprite_Handle    fondo(new Sprite( textures[ID(fondo)].get () ));
        left_limit->set_anchor   (TOP | LEFT);
        left_limit->set_position ({ -50.f, canvas_height });
        right_bar->set_anchor   (TOP | RIGHT);
        right_bar->set_position ({ canvas_width + 100.f, canvas_height });
        fondo->set_position ({ canvas_width / 2.f, canvas_height / 2.f });


        sprites.push_back (fondo);
        sprites.push_back (left_limit);
        sprites.push_back (right_bar);

        // Creation players and bomb

        Sprite_Handle ia_handle(new Sprite( textures[ID(iasub)].get () ));
        Sprite_Handle ia_handle2(new Sprite( textures[ID(iasub)].get () ));
        Sprite_Handle player_handle(new Sprite( textures[ID(player)].get () ));
        Sprite_Handle bomb(new Sprite( textures[ID(bomb)].get () ));
        Sprite_Handle heal(new Sprite( textures[ID(healthsp)].get () ));
        Sprite_Handle heal1(new Sprite( textures[ID(healthsp)].get () ));
        Sprite_Handle heal2(new Sprite( textures[ID(healthsp)].get () ));
        Sprite_Handle lossing(new Sprite( textures[ID(lostsp)].get () ));
        Sprite_Handle pausing(new Sprite( textures[ID(pausesp)].get () ));
        Sprite_Handle pausingicon(new Sprite( textures[ID(pauseiconsp)].get () ));
        Sprite_Handle exiting(new Sprite( textures[ID(exitsp)].get () ));

        //poush backs for the game
        sprites.push_back (pausingicon);
        sprites.push_back (ia_handle);
        sprites.push_back (ia_handle2);
        sprites.push_back (bomb);
        sprites.push_back (heal);
        sprites.push_back (heal1);
        sprites.push_back (heal2);
        sprites.push_back (player_handle);
        sprites.push_back (lossing);
        sprites.push_back (pausing);
        sprites.push_back (exiting);


        // Getters of the srites

        pauseicon=pausingicon.get();
        left_border = left_limit.get ();
        rigth_border = right_bar.get();
        bomba = bomb.get();
        health = heal.get();
        health1 = heal1.get();
        health2 = heal2.get();
        tank   =  ia_handle.get ();
        tank2   =  ia_handle2.get ();
        player  = player_handle.get ();
        lostgame= lossing.get ();
        pauseplay = pausing.get();
        exiticon= exiting.get();



        pausingicon->set_position({68.f , canvas_height -90.f});
        health->set_position({ canvas_width/2+600.f, (canvas_height / 2.f) +300.f });
        health1->set_position({ canvas_width/2+525.f, (canvas_height / 2.f) +300.f });
        health2->set_position( {canvas_width/2+450.f, (canvas_height / 2.f) +300.f});

    }

    // Restart game function

    void Game_Scene::restart_game()
    {
        //hiding the unnecessary sprites to restart the game
        contvidas=0;
        lostgame->hide();
        pauseplay->hide();
        exiticon->hide();
        exiticon->set_position({700.f, 700.f});
        pauseplay->set_position({700.f, 1000.f});
        health2->show();
        health1->show();
        health->show();

        tank_speed=200;
        player_speed=300;
        bomb_speed=-300;
        bomba->set_position ({ canvas_width/2, (canvas_height / 2.f) -600.f });
        tank->set_position ({ (tank->get_width () * 2.f)-1050.f, (canvas_height / 2.f) -300.f});
        tank2->set_position ({ (tank->get_width () * 2.f)-700.f, (canvas_height / 2.f) -300.f});


        tank->set_speed_x  (0.f);
        tank2->set_speed_x  (0.f);
        player->set_position ({ canvas_width/2, (canvas_height / 2.f) +200.f });
        player->set_speed_x  (0.f);

        mouse = false;

        gameplay = WAITING_TO_START;
    }

    //function telling you loss
    void Game_Scene::lost() {
        mouseposition={2000.f,2000.f};
        lostgame->show();
        lostgame->set_position({canvas_width/2, (canvas_height / 2.f)});
        exiticon->set_position({canvas_width/2, (canvas_height / 2.f)-200.f});
       exiticon->show();
        player_speed=0;
        bomb_speed=0;
        tank_speed=0;
        gameplay=LOST;

    }

    //Game Start
    void Game_Scene::start_playing ()
    {

        gameplay = PLAYING;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::run_simulation (float time)
    {
        //update sprites
        for (auto & sprite : sprites)
        {
            sprite->update (time);
        }
        //update player & ia
        update_ai   ();
        update_user ();
        //checks ball collision with the tank
        collision_bomb();
    }

    //Update AI

    void Game_Scene::update_ai ()
    {

        tank->set_speed_x(tank_speed);
        tank2->set_speed_x(tank_speed);
        float random = rand() % (1100-500+1) + 500;
        //collision with the right borden
        if(tank->intersects (*(rigth_border->get_right_x(),rigth_border))){

            //new position of the tank
            tank->set_position({ (tank->get_width () * 2.f)-random, (canvas_height / 2.f) -300.f});

//lifes dissaperance
            contvidas++;
            if (contvidas==1)
                health2->hide();
            else if(contvidas==2)
                health1->hide();
            else if(contvidas==3)
            health->hide();



        }
        else if (tank2->intersects (*(rigth_border->get_right_x(),rigth_border))){
            tank2->set_position({ (tank->get_width () * 2.f)  -random, (canvas_height / 2.f) -300.f});
            contvidas++;
            //lifes dissaperance
            if (contvidas==1)
                health2->hide();
            else if(contvidas==2)
                health1->hide();
            else if(contvidas==3)
                health->hide();

        }
        //calling the function that finishes the game
        if(contvidas==3)
            lost();


    }

    // User update

    void Game_Scene::update_user ()
    {
        if (player->intersects (*left_border))
        {
            // The player stops, when it detecs the collision with the left border

            player->set_position_x (left_border->get_right_x () + player->get_width () / 2.f );
            player->set_speed_x (0);

        }

        else if (player->intersects (*rigth_border))
        {
            // The player stops, when detecs the collision with the right border


            player->set_speed_x (0);
            player->set_position_x (rigth_border->get_right_x () -  player->get_width () / 2.f- 10.f);
        }
        //movement player in the screen
        float delta_y = user_target_y - player->get_position_y ();
        delta_y+=300.f;
        if (mouse && delta_y>0.f )
        {
            // Know where the user is touching
            float delta_x = user_target_x - player->get_position_x ();
            if (delta_x < 0.f) player->set_speed_x (-player_speed); else
            if (delta_x > 0.f) player->set_speed_x (+player_speed);
        }
        else if(delta_y<0.f && mouse && !colddown) {
            // touches a part of the screen to thorw the bomb
            bomba->set_position(player->get_position());
            bomba->set_speed_y(bomb_speed);
            colddown= true;
        }
        if (exiticon->contains(mouseposition)&& gameplay != PLAYING){
            director.run_scene (shared_ptr< Scene >(new Menu_Scene));
        }

// activating the pause
        if (pauseicon->contains(mouseposition)){
            pause();
        }
        //desactivating the pause
        else if (gameplay==PAUSING){
            continuepause();
        }


// restarting game
        if (mouse && gameplay==LOST){
            restart_game();
        }

    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::collision_bomb()
    {


        // Solo si la bola no ha superado alguno de los players, se comprueba si choca con alguno
        // de ellos, en cuyo caso se ajusta su posición para que no los atraviese y se invierte su
        // velocidad en el eje X:

        if(bomba->get_position_y()<(canvas_height / 2.f) -370.f )
            colddown=false;
        if (bomba->intersects(*tank)){
            float random = rand() % (950-400+1) + 400;
            tank->set_position({ (tank->get_width () * 2.f)-random, (canvas_height / 2.f) -300.f});
            tank_speed+=20.f;
            bomb_speed-=10.f;
            player_speed+=10.f;

        }
        else if ( bomba->intersects(*tank2)){
            float random = rand() % (2550-400+1) + 400;
            tank2->set_position({ (tank->get_width () * 2.f)-random, (canvas_height / 2.f) -300.f});
            tank_speed+=20.f;
            bomb_speed-=10.f;
            player_speed+=10.f;
        }


    }

    // ---------------------------------------------------------------------------------------------
//loading textures
    void Game_Scene::render_loading (Canvas & canvas)
    {
        //loading texture id
        Texture_2D * loading_texture = textures[ID(loading)].get ();

        if (loading_texture)
        {
            canvas.fill_rectangle
            (
                { canvas_width * .5f, canvas_height * .5f },
                { loading_texture->get_width (), loading_texture->get_height () },
                  loading_texture
            );
        }
    }

//render of the scene
    void Game_Scene::render_playfield (Canvas & canvas)
    {
        for (auto & sprite : sprites)
        {
            sprite->render (canvas);
        }
    }

}
