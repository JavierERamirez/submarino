/*
 * Menu scene
 * Copyright 2019 Javier Estefania
 */

#ifndef MENU_SCENE_HEADER
#define MENU_SCENE_HEADER

    #include <memory>
    #include <basics/Atlas>
    #include <basics/Canvas>
    #include <basics/Point>
    #include <basics/Scene>
    #include <basics/Size>
    #include <basics/Timer>


    namespace example
    {

        using basics::Atlas;
        using basics::Timer;
        using basics::Canvas;
        using basics::Point2f;
        using basics::Size2f;
        using basics::Texture_2D;
        using basics::Graphics_Context;

        class Menu_Scene : public basics::Scene
        {

            /// states of the scene
            enum State
            {
                LOADING,
                READY,
                FINISHED,
                ERROR
            };
            ///Menu options
            enum Option_Id
            {
                PLAY,
                SCORES,
                HELP,
                EXIT
            };

            struct Option
            {
                const Atlas::Slice * slice;
                Point2f position;
                float   is_pressed;
            };

            static const unsigned number_of_options = 4;

        private:

            State    state;                                     ///Actual state
            bool     suspended;                                 ///checks if is suspended or not
            unsigned canvas_width;
            unsigned canvas_height;
            Timer    timer;                                     ///Checks the time
            Option   options[number_of_options];                ///options number
            std::unique_ptr< Atlas > atlas;                     ///The atlas that contains the different text

        public:

            Menu_Scene();

            basics::Size2u get_view_size () override
            {
                return { canvas_width, canvas_height };
            }

           ///atributes of the scene that have to be fisrt inicialice
            bool initialize () override;

            ///checks if is the main application beign used
            void suspend () override
            {
                suspended = true;
            }

            ///main application in use
            void resume () override
            {
                suspended = false;
            }

            /**
             * Este método se invoca automáticamente una vez por fotograma cuando se acumulan
             * eventos dirigidos a la escena.
             */
            void handle (basics::Event & event) override;

            /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * actualize su estado.
             */
            void update (float time) override;

            /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * dibuje su contenido.
             */
            void render (Graphics_Context::Accessor & context) override;

        private:

            /**
             * Establece las propiedades de cada opción si se ha podido cargar el atlas.
             */
            void configure_options ();

            /**
             * Devuelve el índice de la opción que se encuentra bajo el punto indicado.
             * @param point Punto que se usará para determinar qué opción tiene debajo.
             * @return Índice de la opción que está debajo del punto o -1 si no hay alguna.
             */
            int option_at (const Point2f & point);

        };

    }

#endif
